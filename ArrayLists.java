import java.util.ArrayList;
public class ArrayLists {
    public static void main(String[]args) {

        //declara um ArrayList
        ArrayList<String> cores = new ArrayList<String>();

        //adiciona itens ao ArrayList
        cores.add("Branco");
        cores.add("Vermelho");
        cores.add("Amarelo");
        cores.add("Azul");

        //exibe toods os itens do ArrayList
        System.out.println(cores.toString());

        System.out.println(cores.size());

        System.out.println(cores.get(2));

        cores.remove("Branco");
        System.out.println(cores.toString());

        System.out.println(cores.contains("Azul"));

    }
}