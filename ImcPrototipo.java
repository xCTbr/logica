//ImcProtótipo
//Programa que calcula o IMC
//escreve o valor na tela e informa se o usuário está dentro ou fora do peso ideal.
//@Author: Carlos Tofoli

public class ImcPrototipo {
    public static void main(String[]args){
        
        //cáculo de imc
        //imc = peso / altura²

        //peso
        double pesoEmKg = 70;

        //altura
        double alturaEmMetros = 1.76;

        //imc
        double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
        
        //Testa se o paciente está fora do peso ideal
        //Ideal: 20 a 25
        String msg = imc >= 20 && imc <= 25 ? "Peso IDEAL" : "Fora do peso IDEAL";

        System.out.println("IMC: " + imc);
        System.out.println("Diagnóstico: " + msg);
    }   
}