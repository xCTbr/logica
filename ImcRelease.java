import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

//ImcRelease

//Programa que calcula o IMC
//escreve o valor na tela e informa se 
//o usuário está dentro ou fora do peso ideal.

//Imc Ideal: entre 20 e 25
//Fórmula: imc = peso / altura²

//@Author: Carlos Tofoli

public class ImcRelease {
    public static void main(String[]args) {
        
        //PESO
        String peso = JOptionPane.showInputDialog("Qual o seu peso em Kg?");

        // Converte peso de String para Double (CNVERSÃO ESTATICA)
        double pesoEmKg = Double.parseDouble(peso);

        String altura = JOptionPane.showInputDialog("Qual a sua altura ?").replace(",",".");

        // Converte peso de String para Double (CNVERSÃO ESTATICA)
        double alturaEmMetros = Double.parseDouble(altura);

        //Clculo do imc
        double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
        
        //Classificação do IMC
        String diagnostico = imc >= 20 && imc <= 25 ? "Peso IDEAL" : "Fora do peso IDEAL";
        
        String msg = "IMC: " + imc + "\n" + "Diagnostico: " + diagnostico;

        ImageIcon point = new ImageIcon("sasha.jpg");
        JOptionPane.showMessageDialog(null, msg, "XVIDEOS.COM", JOptionPane.INFORMATION_MESSAGE, point);

        //Mensangens
        //WARNING_MESSAGE
        //INFORMATION_MESSAGE
        //QUESTION_MESSAGE
        //PLAIN_MESSAGE
        //ERROR_MESSAGE
    }
}