public class FluxoFor {
    public static void main (String[]args) {

        //for
        for(int i = 0; i<= 20; i++){
            System.out.println(i);
        }

         //for
        for(int i = 20; i <= 0; i--){
            System.out.println(i);
        }

         //for (Exibindo todos os numeros pares de 0 até 20)
        for(int i = 0; i<= 20; i++){
            if(i%2==0){
            System.out.println(i);
            }
        }

        for(int castigo = 0; castigo<=5; castigo++) {
            System.out.println("Nao devo atirar nos professores");
        }

        ///omprime um quadrado na tela
        // tamanho do quadrado
        int tamanho = 5;
        for(int i = 0; i <= tamanho; i++){
            for(int y = 0; y <= tamanho; y++){
                //pula uma linha
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}