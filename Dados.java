import java.util.Random;
import java.util.Scanner;
/**
 * Programa que sorteia numeros
 *  de 1 a 6 e o usuário deve acertar
 * @Author: Carlos Tofoli 
 */
import javax.swing.JOptionPane;

public class Dados {

    public static void main(String[] args) {

        //Solicita palpite do usuário
        String entrada = JOptionPane.showInputDialog(null, "Chute um numero de 1 a 6 ");
        
        //Converte de String para int
        int chute = Integer.parseInt(entrada);

        //Cria uma variável do tipo Random
        Random sorteio = new Random();
        
        //Gera o nummero aleatorio
        int sorteado = sorteio.nextInt(6)+1;
        
        // Se o usuário inserir um numero incorreto, aparece menssagem de erro
        if(chute > 6 || chute < 1) {
            JOptionPane.showMessageDialog(null,"O numero digitado não condiz com os solicitados ", "Alerta" , JOptionPane.WARNING_MESSAGE);
        }
        
        //Se o chute do usuário for igual ao numero sorteado manssagem "Acertou"
        if(chute == sorteado) {
            JOptionPane.showMessageDialog(null,"Voce acertou!!!");
        
        //Se o chute do usuário for igual ao numero sorteado manssagem "Errou"
        } else {
            JOptionPane.showMessageDialog(null, "Voce errou, o numero era " + sorteado);
        }
    }
}