public class OperadorAtrib {
    public static void main (String[]args) {
        //operadores de atribuição
        int x = 6;

        //soma +3 na variável x
        //x = x + 3;
        //adição
        x += 3; // x = 9

        //subtração
        //x = x - 3
        x -= 3; // x = 6

        //multiplicação
        
        x *= 3; // 6
        
        //divisão
        x /= 3; // 6

        //módulo
        x %= 3; //0 - resto da divisão

        System.out.println(x);
    }
}