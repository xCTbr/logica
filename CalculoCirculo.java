import java.util.Scanner;
public class CalculoCirculo {
    public static void main (String[]args) {

        //double raio = 10;
        Scanner teclado = new Scanner (System.in);
        System.out.println("Qual o valor do raio? ");

        double raio = teclado.nextDouble();

        //diâmetro
        //2r
        double diametro = 2 * raio;
        System.out.println("Diametro: " + diametro);

        //cicunferencia
        //2PIr
        //final double PI = Math.PI;
        double cincunferencia = 2 * Math.PI * raio;
        System.out.println("Circunferencia: " + cincunferencia);

        //area
        //PIr2
        double area = Math.PI * Math.pow(raio, 2);
        System.out.println("Area: " + area);

    }
}