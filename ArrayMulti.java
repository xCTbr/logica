public class ArrayMulti {
    public static void main (String[] args) {

        String[] vetor = {"Ricardo", "Sandra", "Beatriz"};

       // System.out.println (vetor[0]);
        //System.out.println (vetor.length);

        String[][] matriz = {
            {"Ricardo", "M", "DF"},
            {"Sandra", "F", "MG"},
            {"Beatriz", "F", "DF"},
            {"Paulo", "M", "RJ"}
        };
        
        //Ricardo, M, DF
        System.out.print(matriz[0][0] + ", ");
        System.out.print(matriz[0][1] + ", ");
        System.out.println(matriz[0][2] + " ");

        //Sandra, F, MG
        System.out.print(matriz[1][0] + ", ");
        System.out.print(matriz[1][1] + ", ");
        System.out.println(matriz[1][2] + ", ");
        
        //Beatriz, F, DF
        System.out.print(matriz[2][0] + ", ");
        System.out.print(matriz[2][1] + ", ");
        System.out.println(matriz[2][2] + ", ");

        //Paulo, M, Rj
        System.out.print(matriz[3][0] + ", ");
        System.out.print(matriz[3][1] + ", ");
        System.out.println(matriz[3][2] + ", ");

        //total de elementos na matriz
        System.out.print(matriz[0].length);
        System.out.print(matriz[1].length);
        System.out.print(matriz[2].length);
        System.out.print(matriz[3].length);

        //Soma do total de elementos
        //int totalMatriz = matriz[0].length + matriz[1].length []

    }
}