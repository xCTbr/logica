import java.util.ArrayList;

public class FluxoForeach {
    public static void main (String[] args) {
        int[] pares = {2,4,6,8};

        for (int par : pares){
            System.out.println(par);
        }

        String[] nomes = {"Carlos", "Henrique", "Francisco", "Silva", "Tofoli"};

        for (String n : nomes) {
            System.out.println(n);
        }

        ArrayList<Integer> lista = new ArrayList<Integer>();
        for (int i =  0; i <= 10; i++) {
            lista.add(i);
        }

        //imprime todo o arraylist

        for(int valor : lista) {
            System.out.println(valor);
        }
    }
}