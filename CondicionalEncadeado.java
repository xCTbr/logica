public class CondicionalEncadeado {
    public static void main (String[] args) {

        //até 11 anos é criança
        //entre 12 e 18 anos é aborrecente
        //entre 19 e 60 anos Adulto
        //acima de 60 anos Melhor idade

        int idade = 12;

        if (idade <= 11) {
            System.out.println("Criança");
        } else if (idade >= 12 && idade <= 18) {
            System.out.println("Aborrecente");
        } else if (idade >= 19 && idade <= 60) {
            System.out.println("Adulto");
        } else {
            System.out.println("Melhor idade");;
        }

        int nota = 7;
        if (nota >= 7) {
            System.out.println("Aprovado");
        } else {
         System.out.println("Reprovado");
         if (nota >= 6) {
             System.out.println("Mas pode fazer recuperação");
         }
    }
}
}