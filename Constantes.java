public class Constantes {
    public static void main (String[]args){

        // mutavel - variavel
        int populacaoBrasileira = 203429779;
        populacaoBrasileira = 0;
        System.out.println(populacaoBrasileira);

        // imutavel - constante
        final double PI = 3.1415926543;

        final char SEXO_MASCULINO = 'M';
        final char SEXO_FEMININO = 'F';
    }
}