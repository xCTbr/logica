/**
*PROGRAMA QUE ESCREVE UMA MENSAGEM NA TELA
*@AUTHOR Carlos Tofoli( ͡° ͜ʖ ͡°)
*/


public class HelloWorld { //Inicio da classe

    //função principal
    public static void main (String[] args){
    
        //Método que escreve na tela
        System.out.println("\n\" Hello World \"");
    
    }
} //Fim da classe