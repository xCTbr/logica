public class AutoBoxing {
    public static void main(String[]args){

        //boxing = empacotamento
        Integer x = new Integer (555);
        x++;
        System.out.println(x);

        Boolean v = new Boolean ("true");
        //extrair para um tipo primitivo
        if(v) {
            System.out.println("V");
        }else{
            System.out.println("F");
        }
        }

        //recupera o valor de x 
        // unboxing - desempacotar
        //int a = x.intValue();

        // x = 555 + 1 incrementar
        //a++

        // re-empacotar
        //x = new Integer(a);
    }
}