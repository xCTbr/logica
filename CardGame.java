import java.util.Random;

import javax.swing.JOptionPane;

public class CardGame {
    public static void main(String[]args) {
        String[] faces = {
            "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Q", "J", "K" 
        };
        String[] naipes = {
            "Espadas", "Paus", "Copas", "Ouros",
        };
         //seleciona uma carta
        Random random = new Random();        
        int indiceFace = random.nextInt(faces.length);
        String face = faces[indiceFace];
        
        int indiceNaipe = (random.nextInt(naipes.length));
        String naipe = naipes[indiceNaipe];
    
        String carta = "\n\n\n" + face + " de " + naipe + "\n \n \n";
        JOptionPane.showMessageDialog(null, carta);        
    }
}