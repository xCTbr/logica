public class Variaveis{
    public static void main (String[]args){
        
        //declaração de variaveis
        String nome = "Carlos Tofoli";
        
        int idade = 16;

        boolean namorando = true;

        double preco = 12.45;

        float contas = 123.85F;

        char sexo = 'M'; //unicode

        byte b = 127; //cem

        short s = 32767; //32Mil

        int i = 2_000_000_000; //2 bilhões

        long l = 9_000_000_000_000_000_000L; //9 quintilhões

        //pontos flutuantes (casas decimais)

        double d = 1.7976931348623157E+308; //IEEE754

        float f = 123; //float

        // 1 Byte = 8 bits

        byte bb = 0b01010101; //44
        
        //2 Bytes

        short ss = 0b101010101010;

        // cast implícito

        int meuInt = 32;

        short meuShort = 80;
        System.out.println(meuInt); //80

        int meuInt2 = 32;
        long meuLong = 2_000_000_000;

        // cast explícito
        meuInt2 =  (int) meuLong;

    }
}